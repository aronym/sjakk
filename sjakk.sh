#!/bin/bash

# for farget TEXT (eller bakgrunn): '\e[1;FARGEKODEmTEXT\e[0m'
d='\e[91m' #rød skrift
h='\e[37m' #hvit skrift
u='\e[43;' # bakgrunn
b='\e[44m' #blå bakgrunn
r='\e[0m'    #reset

# for brikkenavn
brikkenavn=('din bonde' 'din løper' 'din hest' 'ditt tårn' 'din dronning' 'din konge')

# startposisjoner:
# for tomt felt: X
# brettet settes opp loddrett speilsymmetrisk
brett=(void
  $h\R $h\H $h\B $h\Q $h\K $h\B $h\H $h\R #rad 1
  $h\p $h\p $h\p $h\p $h\p $h\p $h\p $h\p #rad 2
     X    X    X    X    X    X    X    X #rad 3
     X    X    X    X    X    X    X    X #rad 4
     X    X    X    X    X    X    X    X #rad 5
     X    X    X    X    X    X    X    X #rad 6
  $d\p $d\p $d\p $d\p $d\p $d\p $d\p $d\p #rad 7
  $d\R $d\H $d\B $d\Q $d\K $d\B $d\H $d\R #rad 8
) #  a    b    c    d    e    f    g    h #kolonne

# for å tegne brettet
div="echo -e \t \x20 +---+---+---+---+---+---+---+---+"
letters="echo -e \t \x20 \x20 a \x20 b \x20 c \x20 d \x20 e \x20 f \x20 g \x20 h"
srettel="echo -e \t \x20 \x20 h \x20 g \x20 f \x20 e \x20 d \x20 c \x20 b \x20 a"
evenline () {
  echo -e -n '|'$b ${1/X/'\x20'}$b $r'|' ${2/X/'\x20'}$r '|'$b ${3/X/'\x20'}$b $r'|' ${4/X/'\x20'}$r '|'$b ${5/X/'\x20'}$b $r'|' ${6/X/'\x20'}$r '|'$b ${7/X/'\x20'}$b $r'|' ${8/X/'\x20'}$r '|'
}
oddline () {
  echo -e -n '|' ${1/X/'\x20'}$r '|'$b ${2/X/'\x20'}$b $r'|' ${3/X/'\x20'}$r '|'$b ${4/X/'\x20'}$b $r'|' ${5/X/'\x20'}$r '|'$b ${6/X/'\x20'}$b $r'|' ${7/X/'\x20'}$r '|'$b ${8/X/'\x20'}$b $r'|'
}
line () {
  pos=$1
  count=0
  while [ $count -le 8 ]
  do
    echo -e ${brett[$pos]}
    let pos$2
    let count++
  done
}
draw () { # tegn brettet fra hvit vinkel
  clear; echo -e "\n\tSkriv 'quit' om du ønsker å avslutte spillet"
  echo -e "\tSkriv 'quit' etter å ha valgt brikke for å ombestemme deg"
  echo -e "\tSkriv 'inv' hvis motspilleren din har gjort et ugyldig trekk\n\n\n"; $letters
  $div; echo -n -e '\t 8 '; evenline $(line 57 ++); echo ' 8'
  $div; echo -n -e '\t 7 '; oddline $(line 49 ++); echo ' 7'
  $div; echo -n -e '\t 6 '; evenline $(line 41 ++); echo ' 6'
  $div; echo -n -e '\t 5 '; oddline $(line 33 ++); echo ' 5'
  $div; echo -n -e '\t 4 '; evenline $(line 25 ++); echo ' 4'
  $div; echo -n -e '\t 3 '; oddline $(line 17 ++); echo ' 3'
  $div; echo -n -e '\t 2 '; evenline $(line 9 ++); echo ' 2'
  $div; echo -n -e '\t 1 '; oddline $(line 1 ++); echo ' 1'
  $div; $letters; echo -e '\n\n'
}
ward () { # tegn brettet fra svart vinkel
  clear; echo -e "\n\tSkriv 'quit' om du ønsker å avslutte spillet"
  echo -e "\tSkriv 'quit' etter å ha valgt brikke for å ombestemme deg"
  echo -e "\tSkriv 'inv' hvis motspilleren din har gjort et ugyldig trekk\n\n\n"; $srettel
  $div; echo -n -e '\t 1 '; evenline $(line 8 --); echo ' 1'
  $div; echo -n -e '\t 2 '; oddline $(line 16 --); echo ' 2'
  $div; echo -n -e '\t 3 '; evenline $(line 24 --); echo ' 3'
  $div; echo -n -e '\t 4 '; oddline $(line 32 --); echo ' 4'
  $div; echo -n -e '\t 5 '; evenline $(line 40 --); echo ' 5'
  $div; echo -n -e '\t 6 '; oddline $(line 48 --); echo ' 6'
  $div; echo -n -e '\t 7 '; evenline $(line 56 --); echo ' 7'
  $div; echo -n -e '\t 8 '; oddline $(line 64 --); echo ' 8'
  $div; $srettel; echo -e '\n\n'
}

# for å konvertere input på form bokstav\tall til sin verdi i array 'brett'
velgfelt () {
while true
do
  echo -e -n "$1"
  read flt
  flt=$(echo $flt | tr [V] [x])
  if [ $( echo ${flt:0:1} | tr [a-h] [V*] )$( echo ${flt:1:1} | tr [1-8] [V*] ) == 'VV' ]
  then
    felt=$(( $(echo ${flt:0:1} | tr [a-h] [1-8]) + ( ${flt:1:1} - 1 ) * 8 ))
    break
  elif [ $flt == 'quit' ]
  then
    felt='quit'
    break
  elif [ $flt == 'inv' ]
  then
    felt='inv'
    break
  else
    echo -e "Skriv inn et gyldig felt, f.eks. d1 eller h8: \a"
    continue
  fi
done
}

# for å markere brikken som skal flytte
flyttfrafelt () {
frafelt=$1
frafeltbrikke=${brett[frafelt]}
brett[$frafelt]=$u${brett[frafelt]:3:4}
$2
}

# for å flytte brikken
flyttilfelt () {
tilfelt=$1
tilfeltbrikke=${brett[tilfelt]}
brett[$tilfelt]=${brett[frafelt]}
brett[$frafelt]='\e[43mX'
$2
}

# Spillet begynner
draw
turteller=1
rundeteller=0
goback='false'

while true
do
  case $rundeteller in
    0) # Hvitt trekk nr. $(( ( $ + 1 ) / 2 ))
      unset hvittbrett; hvittbrett=(${brett[*]})
      velgfelt '\tHvit spiller: hvilket felt vil du flytte fra? '
      if [ $felt == 'quit' ]; then
        while true; do
          echo -e -n '\tGir du opp? [Y/n] '
          read
          if [ $REPLY == 'Y' ]; then
            echo -e '\tHvit spiller har gitt opp'
            exit
          elif [ $REPLY == 'n' ]; then
            draw
            break
          else
            continue
          fi
        done
      elif [ $felt == 'inv' ]; then
        if [ $goback == 'false' ]; then
          draw
          echo -e '\tDet er ikke mulig å gå tilbake nå'
        else
          goback='false'
          unset brett; brett=(${sortbrett[*]})
          # Slett fra logg #
          let turteller--
          ward
          rundeteller=2
          continue
        fi
      else
        flyttfrafelt $felt draw # markerer valgte brikke
        rundeteller=1
      fi
    ;;
    1)
      velgfelt '\tHvit spiller: hvor ønsker du å flytte '"${brikkenavn[$(echo ${frafeltbrikke:6:1} | tr [pBHRQK] [012345])]}"'? '
      if [ $felt == 'quit' ]; then
        brett[$frafelt]=$frafeltbrikke
        draw
        rundeteller=0
      elif [ $felt == 'inv' ]; then
        if [ $goback == 'false' ]; then
          draw
          echo -e '\tDet er ikke mulig å gå tilbake nå'
        else
          goback='false'
          unset brett; brett=$({sortbrett[*]})
          # Slett fra logg #
          let turteller--
          ward
          rundeteller=2
        fi
      else
        flyttilfelt $felt ward # markerer begge felt
        goback='true'
        brett[$tilfelt]=$frafeltbrikke
        brett[$frafelt]='X'
        # skriv til logg
        let turteller++
        rundeteller=2
      fi
    ;;
    2)
      unset sortbrett; sortbrett=(${brett[*]})
      velgfelt '\tSort spiller: hvilket felt vil du flytte fra? '
      if [ $felt == 'quit' ]; then
        while true; do
          echo -e -n '\tGir du opp? [Y/n] '
          read
          if [ $REPLY == 'Y' ]; then
            echo -e '\tSort spiller har gitt opp'
            exit
          elif [ $REPLY == 'n' ]; then
            ward
            break
          else
            continue
          fi
        done
      elif [ $felt == 'inv' ]; then
        if [ $goback == 'false' ]; then
          draw
          echo -e '\tDet er ikke mulig å gå tilbake nå'
        else
          goback='false'
          unset brett; brett=(${hvittbrett[*]})
          # Slett fra logg #
          let turteller--
          draw
          rundeteller=0
          continue
        fi
      else
        flyttfrafelt $felt ward # markerer valgte brikke
        rundeteller=3
      fi
    ;;
    3)
      velgfelt '\tSort spiller: hvor ønsker du å flytte '"${brikkenavn[$(echo ${frafeltbrikke:6:1} | tr [pBHRQK] [012345])]}"'? '
      if [ $felt == 'quit' ]; then
        brett[$frafelt]=$frafeltbrikke
        ward
        rundeteller=2
      elif [ $felt == 'inv' ]; then
        if [ $goback == 'false' ]; then
          draw
          echo -e '\tDet er ikke mulig å gå tilbake nå'
        else
          goback='false'
          unset brett; brett=$({hvittbrett[*]})
          # Slett fra logg #
          let turteller--
          draw
          rundeteller=0
        fi
      else
        flyttilfelt $felt draw # markerer begge felt
        goback='true'
        brett[$tilfelt]=$frafeltbrikke
        brett[$frafelt]='X'
        # skriv til logg
        let turteller++
        rundeteller=0
      fi
    ;;
  esac
done

echo 'Hvor ønsker du å flytte '"${brikkenavn[$(echo ${frafeltbrikke:6:1} | tr [pBHRQK] [012345])]}"'?'

